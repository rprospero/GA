{-# LANGUAGE DeriveDataTypeable #-}

module GA where

import Data.Data
import Data.Generics.Uniplate.Data
import Data.List (sort)
import Data.Typeable
import Test.QuickCheck

type Directions = [Int]

e1 :: Num a => MultiShell a
e1 = Single 1 [1]
e2 :: Num a => MultiShell a
e2 = Single 1 [2]
e3 :: Num a => MultiShell a
e3 = Single 1 [3]

data MultiShell a =
  Single a Directions |
  ShellSum [MultiShell a]
  deriving (Show, Eq, Data, Typeable)

instance Ord a => Ord (MultiShell a) where
  compare (Single x d1) (Single y d2)
    | d1 == d2 = compare x y
    | d1 < d2 = LT
    | otherwise = GT
  compare (ShellSum a) (ShellSum b) = compare a b
  compare x (ShellSum y) = compare [x] y
  compare (ShellSum x) y = compare x [y]

instance (Data a, Ord a, Num a) => Num (MultiShell a) where
  (ShellSum x) + (ShellSum y) = simple $ ShellSum . sort $ x ++ y
  (ShellSum x) + y = simple $ ShellSum $ x ++ [y]
  x + (ShellSum y) = simple $ ShellSum $ x:y
  x + y = simple $ ShellSum [x, y]

  (Single x d1) * (Single y d2) = simple $ Single (x*y) $ d1 ++ d2
  (ShellSum xs) * (ShellSum ys) =
    simple $ ShellSum . sort $ [x*y | x <- xs , y <- ys]
  x * (ShellSum ys) = simple $ ShellSum $ map (x *) ys
  (ShellSum xs) * y = simple $ ShellSum $ map (* y) xs

  fromInteger x = Single (fromInteger x) []

  abs _ = error "No proper abs for MultiShell"
  signum _ = error "No proper signum for MultiShell"

  negate (Single x d) = Single (negate x) d
  negate (ShellSum xs) = ShellSum $ map negate xs

instance (Data a, Ord a, Fractional a) => Fractional (MultiShell a) where
  fromRational x = Single (fromRational x) []

instance (Arbitrary a) => Arbitrary (MultiShell a) where
  arbitrary = Single <$> arbitrary <*> d
    where
      d = (++) <$> ((++) <$> d1 <*> d2) <*> d3
      d1 = possible 1 <$> arbitrary
      d2 = possible 2 <$> arbitrary
      d3 = possible 3 <$> arbitrary
      possible v x = if x then [v] else []

dot :: (Data a, Ord a, Fractional a) => MultiShell a -> MultiShell a -> MultiShell a
dot a b = simple $ 0.5 * (a*b+b*a)

wedge :: (Data a, Ord a, Fractional a) => MultiShell a -> MultiShell a -> MultiShell a
wedge a b = simple $ 0.5 * (a*b-b*a)

onChange :: Eq a => (a -> a) -> a -> Maybe a
onChange f x
  | f x == x = Nothing
  | otherwise = Just $ f x

simple :: (Ord a, Data a, Num a) => MultiShell a -> MultiShell a
simple = rewrite go
 where
   go (ShellSum []) = Just 0
   go (ShellSum [x]) = Just $ x
   go (Single 0 (_:_)) = Just 0
   go (Single x d) =
     if d == d'
     then Nothing
     else Just $ Single x' d'
     where
       (d', flp) = simplify' d
       x' = if flp
         then negate x
         else x
   go (ShellSum xs) = ShellSum <$> onChange condense (sort xs)

condense :: (Num a, Eq a) => [MultiShell a] -> [MultiShell a]
condense = merge go
  where
    go (Single 0 _) y = Just y
    go (Single x d1) (Single y d2)
      | d1 == d2 = Just $ Single (x+y) d1
      | otherwise = Nothing
    go _ _ = Nothing

merge :: (a -> a -> Maybe a) -> [a] -> [a]
merge f (a:b:xs) =
  case f a b of
    Nothing -> a:merge f (b:xs)
    Just y -> merge f (y:xs)
merge _ xs = xs

simplify' :: Directions -> (Directions, Bool)
simplify' xs = go xs False []
  where
    go (a:b:ys) flp acc
      | a == b = go ys flp acc
      | a > b = go (b:a:ys) (not flp) acc
      | otherwise = go (b:ys) flp $ a:acc
    go [b] flp acc = (reverse $ b:acc, flp)
    go [] flp acc = (reverse acc, flp)

conj :: (Num a) => MultiShell a -> MultiShell a
conj (ShellSum xs) = ShellSum $ conj <$> xs
conj (Single x d) = Single x' d
  where
    x' = if (length d `div` 2) `mod` 2 == 0
      then x
      else negate x
