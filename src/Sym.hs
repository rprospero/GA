{-# LANGUAGE DeriveDataTypeable #-}

module Sym where

import Data.Data
import Data.Generics.Uniplate.Data
import Data.List (sort)
import Data.Typeable

data Sym a =
  Val a
  | Sym String
  | Sum [Sym a]
  | Product [Sym a]
  deriving (Show, Eq, Data, Typeable, Ord)

instance Num a => Num (Sym a) where
  Sum xs + Sum ys = Sum $ xs ++ ys
  Sum xs + y = Sum $ xs ++ [y]
  x + Sum ys = Sum $ [x] ++ ys
  Val x + Val y = Val $ x+y
  x + y = Sum [x, y]

  Product xs * Product ys = Product $ xs ++ ys
  Product xs * y = Product $ xs ++ [y]
  x * Product ys = Product $ [x] ++ ys
  Val x * Val y = Val $ x * y
  x * y = Product [x, y]

  abs _ = error "No proper abs for MultiShell"
  signum _ = error "No proper signum for MultiShell"

  negate (Val x) = Val $ negate x
  negate (Product xs) = Product $ [-1] ++ xs
  negate x = Product [-1, x]

  fromInteger x = Val $ fromInteger x

instance Fractional a => Fractional (Sym a) where
  fromRational x = Val $ fromRational x
