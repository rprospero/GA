{-# LANGUAGE DeriveDataTypeable #-}

module Lib
    ( someFunc
    ) where

import GA
import Sym

example :: MultiShell Double
example = simple $ e2 * e2

z :: MultiShell (Sym Double)
z = 1

notz :: MultiShell (Sym Double)
notz = negate $ e1 * e2

i :: MultiShell (Sym Double)
i = e1 * e2 * e3

someFunc :: IO ()
someFunc = do
  print $ z * conj z
  print $ dot z $ conj z
  print $ wedge z $ conj z

  print ""

  print $ notz * conj notz
  print $ dot notz $ conj notz
  print $ wedge notz $ conj notz

  print ""

  print $ e1 * z * e3
  print $ e2 * z * e3
  print $ e3 * z * e3

  print ""

  print $ e1 * notz * e3
  print $ e2 * notz * e3
  print $ e3 * notz * e3

  print ""

  print $ dot e1 $ conj z * e3 * z
  print $ dot e2 $ conj z * e3 * z
  print $ dot e3 $ conj z * e3 * z
  print $ dot e1 $ conj notz * e3 * notz
  print $ dot e2 $ conj notz * e3 * notz
  print $ dot e3 $ conj notz * e3 * notz
